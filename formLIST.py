#!/usr/bin/python
#-*- coding: utf-8 -*-

import csv
import os


def listing():
    # Checking input arguments and folder:
    assert os.path.isdir('Listing'), "Folder 'Listing' does not exist!"
    assert os.path.isfile("Listing/Gags.txt"), "File 'Gags.txt' does not exist!"
    assert os.path.isfile("Listing/Mults_Human.txt"), "The file " \
                                        "'Mults_Human.txt' does not exist!"
    assert os.path.isfile("Listing/Mults_Mouse.txt"), "The file " \
                                        "'Mults_Mouse.txt' does not exist!"
    assert os.path.isfile("Listing/Mults_Others.txt"), "The file " \
                                        "'Mults_Others.txt' does not exist!"
    assert os.path.isfile("Listing/PFrags.txt"), "The file 'PFrags.txt' does " \
                                        "not exist!"

    # Function to open data and file:
    def open_data(name):
        with open(name,"r") as data_file:
            reader = csv.reader(data_file, delimiter="\t")
            return list(reader)

    # Function read data:
    def read_data(liste):
        Biomolecule = []
        for i in range(len(liste) - 1):
            if (len(liste[i]) > 0) :
                donne = []
                if (liste[i][0][:12] == "BioMolecule "):
                    j = i
                    while (j < len(liste)):
                        donne.append(liste[j])
                        j += 1
                    Biomolecule.append(donne)
        return Biomolecule

    # Function protein list:
    def listeprot(liste, list_prot=[]):
        for i in range(len(liste)):
            donne1 = []
            donne2 = []
            donne3 = []
            if len(liste[i]) > 0:
                if liste[i][0][:12] == "BioMolecule ":
                    j = i
                    while j < len(liste) and len(liste[j]) > 0:
                        if (liste[j][0][:9] == "Component"
                              and liste[i][0][13:18] != liste[j][1][2:7]
                              and liste[j][1][2:8] != "PFRAG_"
                              and liste[j][1][2:7]!="MULT_"):
                            i2 = 13
                            j2 = 2
                            while (liste[i][0][i2] != '"'
                                    and i2 < len(liste[i][0])):
                                i2 += 1
                            while (liste[j][1][j2] != "-"
                                    and j2 + 1 < len(liste[j][1])):
                                j2 += 1
                            donne1.append([liste[i][0][13:i2],
                                           liste[j][1][2:j2],
                                           specie(i, liste)])
                            k = i
                            while k < len(liste) and len(liste[k]) > 0:
                                if liste[k][0][:8] == "EBI_xref":
                                    k2 = 2
                                    while (liste[k][1][k2] != '"' 
                                           and k2 < len(liste[k][1])):
                                        k2 += 1
                                    donne2.append([liste[k][1][2:k2],
                                                   liste[j][1][2:j2],
                                                   specie(i, liste)])
                                if liste[k][0][:13] == "Multimer_Name":
                                    k3 = 2
                                    while (liste[k][1][k3] != '"' 
                                          and k3 < len(liste[k][1])):
                                        k3 += 1
                                    donne3.append([liste[k][1][2:k3],
                                                   liste[j][1][2:j2],
                                                   specie(i, liste)])
                                k += 1
                        elif (liste[i][0][:19] == "BioMolecule \"PFRAG_" 
                              and liste[j][0][0:10] == "Belongs_to" 
                              and liste[j][0][12:18] != "PFRAG_" 
                              and liste[j][0][12:19] != " PFRAG_"):
                            donne1.append([liste[i][0][13:-1],
                                           liste[j][0][12:-1],
                                           specie_other(i, liste)])
                        j += 1
                for l1 in range(len(donne1)):
                    if donne1 != [] and not(donne1 in list_prot):
                        list_prot.append(donne1[l1])
                for l2 in range(len(donne2)):
                    if donne2 != [] and not(donne2 in list_prot):
                        list_prot.append(donne2[l2])
                for l3 in range(len(donne3)):
                    if donne3 != [] and not(donne3 in list_prot):
                        list_prot.append(donne3[l3])
        return list_prot

    def specie(row1, list_data):
        i = row1
        taxid = 'unknown'
        while i < len(list_data) and len(list_data[i]) > 0:
            if list_data[i][0][:10] == "In_Species":
                j1 = 0
                while (list_data[i][1][j1] != '"' 
                       and j1 < len(list_data[i][1]) - 1):
                    j1 += 1
                j1 += 1
                j2 = j1
                while (list_data[i][1][j2] != '"'
                       and j2 < len(list_data[i][1])-1):
                    j2 += 1
                taxon = [['chicken', '9031'],
                         ['human', '9606'],
                         ['dog', '9615'],
                         ['taurus', '9913'],
                         ['mouse', '10090'],
                         ['rat', '10116'],
                         ['pig', '9823'],
                         ['sheep', '9940']]
                for k in range(len(taxon)):
                    if list_data[i][1][j1:j2] == taxon[k][1]:
                        taxid = taxon[k][0]
            i += 1
        return taxid

    # Function protein list for Mult_Other:
    def listeprotMO(liste, list_prot):
        for i in range(len(liste)):
            donne1 = []
            donne2 = []
            if len(liste[i]) > 0:
                if liste[i][0][:12] == "BioMolecule ":
                    j = i
                    while j < len(liste) and len(liste[j]) > 0:
                        j1 = len(liste[i][0]) - 1
                        while liste[i][0][j1] != '"' and j1 > 13:
                            j1 -= 1
                        if liste[j][0][:9] == "Component":
                            j3 = 9
                            while (liste[j][0][j3] != '"' 
                                   and j3 < len(liste[j][0]) - 1):
                                j3 += 1
                            j3 += 1
                            j2 = len(liste[j][0]) - 1
                            while liste[j][0][j2] != '"' and j2 > 13:
                                j2 -= 1
                            donne1.append([liste[i][0][13:j1],
                                           liste[j][0][j3:j2],
                                           specie_other(i, liste)])
                            k = i
                            while k < len(liste) and len(liste[k]) > 0:
                                if liste[k][0][:9] == "EBI_XREF ":
                                    k1 = len(liste[k][0]) - 1
                                    while liste[k][0][k1] != '"' and k1 > 9:
                                        k1 -= 1
                                    donne2.append([liste[k][0][10:k1],
                                                   liste[j][0][j3:j2],
                                                   specie_other(i, liste)])
                                k += 1
                        j += 1
                for l1 in range(len(donne1)):
                    if donne1 != [] and not(donne1 in list_prot):
                        list_prot.append(donne1[l1])
                for l2 in range(len(donne2)):
                    if donne2 != [] and not(donne2 in list_prot):
                        list_prot.append(donne2[l2])
        return list_prot


    def specie_other(row1, list_data):
        i = row1
        taxid = 'unknown'
        while i < len(list_data) and len(list_data[i]) > 0:
            if list_data[i][0][:10] == "In_Species":
                j1 = 10
                while (list_data[i][0][j1] != '"' 
                    and j1 < len(list_data[i][0]) - 1):
                    j1 += 1
                j1 += 1
                j2 = j1
                while (list_data[i][0][j2] != '"' 
                       and j2 < len(list_data[i][0]) - 1):
                    j2 += 1
                taxon = [['chicken', '9031'],
                         ['human', '9606'],
                         ['dog', '9615'],
                         ['taurus', '9913'],
                         ['mouse', '10090'],
                         ['rat', '10116'],
                         ['pig', '9823'],
                         ['sheep', '9940']]
                for k in range(len(taxon)):
                    if list_data[i][0][j1:j2] == taxon[k][1]:
                        taxid = taxon[k][0]
            i += 1
        return taxid
        
    # Open data file
    dataGAG = open_data("Listing/Gags.txt")
    dataMH = open_data("Listing/Mults_Human.txt")
    dataMM = open_data("Listing/Mults_Mouse.txt")
    dataMO = open_data("Listing/Mults_Others.txt")
    dataPF = open_data("Listing/PFrags.txt")

    # Read data
    readGAG = read_data(dataGAG)
    readMH = read_data(dataMH)
    readMM = read_data(dataMM)
    readMO = read_data(dataMO)
    readPF = read_data(dataPF)

    # Create id list
    # TODO revoir/renommer les listes
    liste = listeprot(readGAG[0])
    liste = listeprot(readMH[0], liste)
    liste = listeprot(readMM[0], liste)
    liste = listeprotMO(readMO[0], liste)
    liste = listeprot(readPF[0], liste)

    # Create output file:
    with open("Listing/List_id.csv", "w", encoding='UTF-8') as f:
        for i in range(len(liste)):
                writing = ""
                for j in range(len(liste[i])):
                    writing += str(liste[i][j]) + "\t"
                f.write(writing[:-1] + "\n")
    f.close()


listing()
