import argparse
import csv
import os
import re

def open_data(file: str):
    """
    :param file: file path
    :return: a readable file for the program
    """
    with open(file, "r") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        return list(reader)

parser = argparse.ArgumentParser()
parser.add_argument("infile", type=str, help="csv input file", metavar='INFILE')
parser.add_argument("matrisome", type=str, help="matrisome file")
args = parser.parse_args()


# matrisome_file = open_data(args.infile)
#
# id_list = []
#
# for line in matrisome_file:
#     id_col = line[7]
#     id_col = id_col.split(':')
#     id_list = id_list + id_col
#
# with open('matrisome_id.csv', "w", encoding='UTF-8') as outcsv:
#     for elt in id_list:
#         outcsv.write(f"{elt}\n")
#     outcsv.close()

infile = open_data(args.infile)
matrisome = open_data(args.matrisome)

matrisome[:] = [x[0] for x in matrisome if x != []]

matrisome_node = []

# filter
for i in range(len(infile)):
    for j in range(3,5):
        if infile[i][j] in matrisome:
            print(infile[i][j], infile[i][j-2])
            matrisome_node.append(infile[i][j-2])

filename = 'matrisome_filter.csv'


with open(filename, "a", encoding='UTF-8') as outcsv:
    for node in matrisome_node:
        outcsv.write(f"{node}\n")
    outcsv.close()
