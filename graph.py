import collections
from collections import Counter
from collections import defaultdict
import csv
from decimal import Decimal
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import sys
from scipy.stats import gaussian_kde
from statistics import mean
from statistics import stdev

# change max recursion depth (for the articulation point detection)
sys.setrecursionlimit(10000)


def open_data(file: str):
    """
    :param file: file path
    :return: a readable file for the program
    """
    with open(file, "r") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        return list(reader)


def print_dict(d):
    """
    :param d: dict
    Print the "key : value" pairs
    """
    for key, value in sorted(d.items()):
        print(key, value)


def get_values_from_dict(d):
    """
    :param d: dict
    :return: list containing the dict values
    """
    value_list = []
    for k in d:
        value_list.append(d[k])
    return value_list


def print_occurrence_dict(data_list):
    """
    :param data_list: list
    Print an ordered dict of the values and their occurrences in the data_list
    """
    occ = Counter(data_list)  # occurrence dict {value:occurrence}
    occ = collections.OrderedDict(sorted(occ.items()))  # ordered dict based on the keys

    print("========VALUES=========")
    print_dict(occ)


def write_csv_from_ordered_dict(odic, path):
    """
    :param odic: Ordered dictionnary
    :param path: output file path
    Write the OrderedDict into a csv file
    """
    with open(path, "w") as outfile:
        fieldnames = ['node id', 'node name']
        writer = csv.DictWriter(outfile, fieldnames=fieldnames, delimiter="\t")
        writer.writeheader()
        for k, v in odic.items():
            writer.writerow({'node id': k, 'node name': v})
        outfile.close()


class Graph(object):

    def __init__(self, tabulated_file, old_tab=False, infile=None, tab_filename=None):
        self.infile = infile        # mitab file path
        self.tab_filename = tab_filename    # tabulated file path
        self.tab_file_old = tabulated_file
        self.tab_file = tabulated_file      # tabulated file path
        self.result_folder = ""     # result folder path
        self.base_filename = ""     # filename base name (no folder, no extension)
        self.node_list = []
        self.node_set = set()       # set of node names
        self.id_name = {}           # dict w/ the node id and names {id: name}
        self.nnode = None           # number of nodes
        self.nedge = None           # number of edges
        self.density = None         # density of the graph (global clustering coefficient)
        self.node_neighbour = {}    # dict w/ a set of neighbours for each node
        self.node_degree = {}       # dict w/ the degree of each node
        self.node_coef = {}         # dict w/ the local clustering coef for each node
        self.big_dict = {}          # contains degree, coef and a set of neighbours for each node
        self.connected_component = []       # list of connected components
        self.nb_connected_component = None  # number of connected components in the graph
        self.equivalence = []               # list of equivalence classes
        self.equi_surface = []              # list of equivalence classes' surface
        self.equi_size_degree = []          # list of tuples (class size, degree, class, neighbours)
        self.iso_tab_file = []              # tabulated file containing the lines w/ isoforms
        self.iso_list = []                  # tuple list (iso alias, iso id)
        self.ap = {}                # dict containing: {ap: (degree, nb of associated conn comp)}
        self.children = {}          # dict containing the number of children of each node in DFS

        self.time = 0               # counter for DFS (articulation point detection)

        # filter for interaction between a node and itself
        self.tab_file[:] = [x for x in self.tab_file if x[1] != x[2]]
        self.tab_file_old[:] = [x for x in self.tab_file_old if x[1] != x[2]]

        # {(alias, id)}
        self.node_set = {(x[j], x[j+2]) for x in self.tab_file for j in range(1, 3)}

        # set result folder path and the base filename
        base = os.path.basename(self.infile)
        folder_prefix = os.path.splitext(base)[0]
        self.result_folder = f"./{folder_prefix}/"
        base = os.path.basename(self.tab_filename)
        self.base_filename = os.path.splitext(base)[0]

        # set a unique name for each node
        name_list = []
        for node in self.node_set:
            name = node[0]
            i = 2
            while name in name_list:
                name = f"{node[0]}_{i}"
                i += 1
            # {id: name}
            self.id_name[node[1]] = name
            name_list.append(name)
        # order the dict
        self.id_name = collections.OrderedDict(sorted(self.id_name.items()))
        # write the dict into an output file
        node_file = f"{self.result_folder}{self.base_filename}_node_name.csv"
        node_file = re.sub("_tab", "", node_file)
        write_csv_from_ordered_dict(self.id_name, node_file)

        # set of node names
        self.node_set = {self.id_name[x] for x in self.id_name}
        self.node_list = sorted(list(self.node_set))
        
        # replace the alias w/ the new node names
        if old_tab:
            self.tab_file = self.tab_file_old[:]
            for i in range(len(self.tab_file_old)):
                for j in range(3, 5):
                    ident = self.tab_file_old[i][j]
                    self.tab_file[i][j-2] = self.id_name[ident]
            # write a tabulated file w/ the new node names
            print("=> Writing new tabulated file...")
            filename = f"{self.result_folder}{self.base_filename}_new.csv"
            with open(filename, 'w') as outfile:
                writer = csv.writer(outfile, delimiter="\t")
                [writer.writerow(r) for r in self.tab_file]
            print("=> Writing new tabulates file: done")
            # rename the new tab to tabfile (replace the old tab file)
            os.rename(self.tab_filename, f"{os.path.splitext(self.tab_filename)[0]}_old.csv")
            os.rename(filename, self.tab_filename)

        # calculate the number of nodes and edges in the graph
        self.nnode = len(self.node_set)
        self.nedge = len(self.tab_file)
        if (self.nnode * (self.nnode-1)) != 0:
            self.density = (2*self.nedge) / (self.nnode * (self.nnode-1))
        else:
            self.density = 0
        self.density = '%.3E' % Decimal(self.density)

    def set_neighbour_degree(self):
        """
        Set 2 dict: {node:{neighbours}} and {node:degree}, with neighbours being a list of
        neighbour nodes, and degree being the degree of the node (int).
        """
        print("=> Setting neighbours and degrees...")
        dict_node_degree = {}
        dict_node_neighbour = {}
        for prot in self.node_set:  # prot = key
            partner = set()  # partner set for prot
            for i in range(len(self.tab_file)):
                # go through the aliases
                for j in range(1, 3):
                    # alias
                    k = self.tab_file[i][j]
                    # add non-self-self interaction partner if not already in partner set
                    if (k == prot and j == 1 and self.tab_file[i][2] not in partner and
                            self.tab_file[i][2] != k):
                        partner.add(self.tab_file[i][2])
                    if (k == prot and j == 2 and self.tab_file[i][1] not in partner and
                            self.tab_file[i][1] != k):
                        partner.add(self.tab_file[i][1])
            # add node: {partner set} in the dict
            dict_node_neighbour[prot] = partner
            # add node: degree in dict
            dict_node_degree[prot] = len(partner)
        self.node_neighbour = dict_node_neighbour
        self.node_degree = dict_node_degree
        print("=> Setting neighbours and degrees: done")

    # Local clustering coefficient
    def get_nb_edge_between_neighbour(self, node):
        """
        :param node: targeted node
        :return: the number of edges between the node's neighbours
        """
        ns = self.node_neighbour[node]
        result = 0
        for n in ns:
            # count the number of edges between the neighbours of node
            # intersection between the neighbours of n and the neighbours of node
            result += len(self.node_neighbour[n] & ns) / 2
        return result

    def get_local_coefficient(self, node):
        """
        :param node: targeted node
        :return: the local clustering coefficient for the node
        """
        # the number of edges between the neighbours of node
        nx = self.get_nb_edge_between_neighbour(node)
        # the node's degree
        k = len(self.node_neighbour[node])
        if k * (k - 1) == 0:
            result = 0
        else:
            result = (2 * nx) / (k * (k - 1))
        return result

    def set_local_coef(self):
        """
        Set the self.node_coef, dict {node:local_coef}
        Calculate the number of edge between the neighbours for one node, the local coefficient of
        clustering, and store the coefficient in a dict for all the nodes.
        A local coefficient equal to 0 can be explained by: 1) a node only has one neighbour 2) the
        neighbours do not have any edge between them.
        formula used:
        (2 * nb of edges between neighbours) / (nb of neighbours * (nb of neighbours - 1))
        """
        if self.node_neighbour == {} or self.node_degree == {}:
            self.set_neighbour_degree()
        print("=> Setting local coef...")
        result = {}
        for node in self.node_neighbour:
            coef = self.get_local_coefficient(node)
            result[node] = coef
        self.node_coef = result
        print("=> Setting local coef: done")

    # Create a subtab from a protein list
    def tab_from_prot_list(self, protein_list):
        """
        :param protein_list: list of protein alias
        :return: a tabulated file containing the lines with the proteins from the input list
        """
        new_tabfile = []
        # keep the lines that have protein from the list
        for line in self.tab_file:
            if line[1] in protein_list or line[2] in protein_list:
                new_tabfile.append(line)
        return new_tabfile

    def write_tab_from_prot_list(self, protein_list):
        print("=> Writing tabulated file...")

        tab = self.tab_from_prot_list(protein_list)

        # choose output file names
        base = os.path.basename(self.tab_filename)      # file name without the folder path
        filename_prefix = os.path.splitext(base)[0]     # file name without the extension
        base = os.path.basename(self.infile)
        folder_prefix = os.path.splitext(base)[0]
        folder_name = "./" + folder_prefix + "/"        # folder containing the result file
        tab_filename = folder_name + filename_prefix + "_trouble_shooting.csv"
        asp_filename = folder_name + filename_prefix + "_trouble_shooting.lp"
        asp_filename = re.sub("_tab", "", asp_filename)

        # write in output files
        with open(tab_filename, "w", encoding='UTF-8') as tab_out:
            with open(asp_filename, "w") as asp_out:
                for i in range(len(tab)):
                    # write in the tabulated file
                    writing = ""
                    for j in range(len(tab[i])):
                        writing += str(tab[i][j]) + "\t"
                    tab_out.write(writing[:-1] + "\n")
                    # write in asp file
                    asp_line = 'edge("' + tab[i][1] + '","' + tab[i][2] + '").'
                    asp_out.write(asp_line + "\n")
                asp_out.close()
            tab_out.close()
        print("=> Writing tabulated file: done")

    # Connected components
    def set_connected_comp(self):
        """
        modified version of:
        https://breakingcode.wordpress.com/2013/04/08/finding-connected-components-in-a-graph/
        Identify and store the separate connected components. Store the number of connected
        components.
        """
        if self.node_neighbour == {} or self.node_degree == {}:
            self.set_neighbour_degree()
        print("=> Setting connected components...")
        # List of connected components found. The order is random.
        result = []
        # Make a copy of the set, so we can modify it.
        nodes = self.node_set
        # Iterate while we still have nodes to process.
        while nodes:
            # Get a random node and remove it from the global set.
            n = nodes.pop()
            # This set will contain the next group of nodes connected to each other.
            group = {n}
            # Build a queue with this node in it.
            queue = [n]
            # Iterate the queue. When it's empty, we finished visiting a group of connected nodes.
            while queue:
                # Consume the next item from the queue.
                n = queue.pop(0)
                # Fetch the neighbors.
                neighbours = self.node_neighbour[n]
                # Remove the neighbors we already visited (are in group).
                neighbours.difference_update(group)
                # Remove the remaining nodes from the global set.
                nodes.difference_update(neighbours)
                # Add them to the group of connected nodes.
                group.update(neighbours)
                # Add them to the queue, so we visit them in the next iterations.
                queue.extend(neighbours)
            # Add the group to the list of groups.
            result.append(group)
        self.connected_component = result
        self.nb_connected_component = len(self.connected_component)
        print("=> Setting connected components: done")

    def print_connected_comp(self):
        """
        Print for each connected component, the sorted components.
        """
        number = 1
        for components in self.connected_component:
            names = sorted(components)
            names = ", ".join(names)
            print(f"Group #{number}: {names}")
            number += 1

    def write_connected_comp(self):
        """
        Create a tabulated file containing the connected components.
        """
        print("=> Writing connected component file...")
        filename_prefix = self.base_filename
        filename_prefix = re.sub("_tab", "", filename_prefix)
        conn_filename = f"{self.result_folder}{filename_prefix}_connected_components.csv"
        with open(conn_filename, 'w') as conn_out:
            number = 1
            line = "group\tgroup_size\tgroup_elts"
            conn_out.write(line + "\n")
            for group in self.connected_component:
                line = ""
                # add group id
                line += f"group #{number}\t"
                # add group size
                line += str(len(group)) + "\t"
                # add group_elts column
                tmp = ""
                for elt in group:
                    tmp += elt + ", "
                tmp = tmp[:-2]
                line += tmp + "\t"
                conn_out.write(line + "\n")
                number += 1
            conn_out.close()

        print("=> Writing connected component file:done")

    def select_tab_comp(self, component_list, neighbour=False, origin_tab=None):
        """
        :param component_list: a list of the elements of a connected component
        :param neighbour: include the neighbours of each node in the component list
        :param origin_tab: origin data (for example data used to generate the percent files),
        used to select the neighbours of nodes.
        :return: a tabulated file containing the interaction lines with elements from component_list
        """
        selected_tab = []
        if neighbour:   # include the neighbours of component_list
            for i in range(len(origin_tab)):
                if origin_tab[i][1] in component_list or origin_tab[i][2] in component_list:
                    selected_tab.append(origin_tab[i])
        else:
            for i in range(len(self.tab_file)):
                if self.tab_file[i][1] in component_list and self.tab_file[i][2] in component_list:
                    selected_tab.append(self.tab_file[i])
        return selected_tab

    def choose_small_folder(self):
        """
        Choose a folder name that will contain all the result files, create new folder if already
        exists.
        """
        # suffix for the small folder name
        suffix = os.path.splitext(os.path.basename(self.tab_filename))[0]
        suffix = suffix.split("_tab")[-1]
        # folder containing the results
        big_folder_name = "./" + os.path.splitext(os.path.basename(self.infile))[0] + "/"
        base_small_folder_name = big_folder_name + "connected_component" + str(suffix)
        small_folder_name = big_folder_name + "connected_component" + str(suffix)
        # create folder
        i = 1
        while os.path.exists(small_folder_name):
            small_folder_name = f"{base_small_folder_name}_{i}"
            i += 1
        return small_folder_name

    def write_tab_comp(self, component_list, small_folder_name, i, neighbour=False,
                       origin_data=None):
        """
        :param component_list: a list of the elements of a connected component
        :param small_folder_name: folder that will contain all the result files
        :param i: number in output filename
        :param neighbour: include the neighbours of each node in the component list
        :param origin_data: bigger original data if the file is a smaller tab
        Create a tabulated file in the result folder
        """
        origin_tab = None
        if origin_data is not None:
            origin_tab = open_data(origin_data)

        # select data to be written into the file
        tab = self.select_tab_comp(component_list, neighbour, origin_tab)

        # create the small folder for the connected component files + a tree directory
        if not os.path.exists(small_folder_name):
            os.makedirs(small_folder_name + "/tree")
        # filename without the folder path and without the extension
        file_name = os.path.splitext(os.path.basename(self.tab_filename))[0]
        # create file path
        if neighbour:
            tab_filename = f"{small_folder_name}/{file_name}_{i}_neighbour.csv"
            asp_filename = f"{small_folder_name}/{file_name}_{i}_neighbour.lp"

        else:
            tab_filename = f"{small_folder_name}/{file_name}_{i}.csv"
            asp_filename = f"{small_folder_name}/{file_name}_{i}.lp"
        # write in the tabulated file
        with open(tab_filename, "w", encoding='UTF-8') as tab_out:
            with open(asp_filename, "w", encoding='UTF-8') as asp_out:
                for i in range(len(tab)):
                    # write tab file
                    line = ""
                    for j in range(len(tab[i])):
                        line += str(tab[i][j]) + "\t"
                    tab_out.write(line[:-1] + "\n")
                    # write in asp file
                    asp_line = 'edge("' + tab[i][1] + '","' + tab[i][2] + '").'
                    asp_out.write(asp_line + "\n")
            tab_out.close()
        return tab_filename, asp_filename

    # Equivalence
    def set_equivalence(self):
        """
        Set the list of the equivalence node groups.
        """
        if self.node_neighbour == {} or self.node_degree == {}:
            self.set_neighbour_degree()
        print("=> Setting equivalence...")
        # list of groups
        result = []
        # copy of the nodes
        nodes = sorted(self.node_neighbour)
        # iterate while there are still nodes to process
        while nodes:
            # consume first node
            n = nodes.pop(0)
            # group contains the nodes w/ the same neighbours
            group = [n]
            # list of neighbours
            list_n = sorted(self.node_neighbour[n])
            other_nodes = nodes
            # compare the neighbour list of the node w/ the other node's neighbour list
            for on in other_nodes:
                other_list = sorted(self.node_neighbour[on])
                if list_n == other_list:
                    # add the other node to the equivalence group
                    group.append(on)
                    # remove the other node from nodes
                    nodes[:] = [x for x in nodes if x != on]
            # add group to the list of groups
            result.append(group)
        self.equivalence = result
        print("=> Setting equivalence: done")

    def set_equi_surface(self):
        """
        Set the surface for each equivalence class (= class size * class elt degree)
        """
        if not self.equivalence:
            self.set_equivalence()
        print("Setting equivalence surface...")
        equi_1 = [x for x in self.equivalence if len(x) > 1]
        # store the size of each equivalence class, the elements' degree, the class and the
        # neighbours
        equi_size_degree = [(len(x), self.node_degree[x[0]], x, self.node_neighbour[x[0]]) for x in
                            equi_1]
        # store the surface of each equivalence class
        self.equi_surface = [x[0]*x[1] for x in equi_size_degree]
        self.equi_size_degree = [(len(x), self.node_degree[x[0]], x, self.node_neighbour[x[0]],
                                  len(x)*self.node_degree[x[0]]) for x in equi_1]
        print("Setting equivalence surface: done")

    def print_equivalence(self):
        """
        Print the equivalence groups.
        """
        number = 1
        for group in self.equivalence:
            if len(group) > 1:
                names = sorted(group)
                names = ", ".join(names)
                print(f"Group #{number}: {names}")
                number += 1

    # TODO redo the function so it prints the interactors of the biggest equi class
    def print_interesting_equivalence(self):
        """Count and print the equivalence classes w/ a surface > mean + 2 * standard deviation"""
        if not self.equi_surface:
            self.set_equi_surface()
        # the equivalence classes bigger than 1 list
        equi_1 = [x for x in self.equivalence if len(x) > 1]
        # store the surfaces of interest
        threshold = mean(self.equi_surface) + 2*stdev(self.equi_surface)
        print("threshold:", round(threshold, 3))
        interesting_surface = [(i, s) for i, s in enumerate(self.equi_surface) if s > threshold]

        # there is only one interesting (big enough) surface
        if len(interesting_surface) == 1:
            print("There is 1 equivalence class.")
            equi_class = equi_1[interesting_surface[0][0]]
            print(f"equi class #1: {equi_class}")
        # there are multiple equivalence classes w/ an interesting surface
        elif len(interesting_surface) > 1:
            print(f"There are {len(interesting_surface)} equivalence classes.")
            for i, elt in enumerate(interesting_surface):
                equi_class = equi_1[elt[0]]
                surface = self.equi_surface[elt[0]]
                neighbours = self.node_neighbour[equi_class[0]]
                degree = self.node_degree[equi_class[0]]
                print(f"equi class #{i+1}: {equi_class}, {surface}")
                print(f"neighbours #{i+1}: {neighbours}, {degree}")

    def write_equivalence(self):
        """
        Create a tabulated file containing: the group number, its size, the group elements and
        the neighbour list of those elements.
        """
        print("=> Writing equivalence file...")
        filename_prefix = self.base_filename
        filename_prefix = re.sub("_tab", "", filename_prefix)
        equi_filename = f"{self.result_folder}{filename_prefix}_equivalence.csv"
        with open(equi_filename, 'w') as equi_out:
            number = 1
            line = "group_size\tgroup_elts\tlist_size\tneighbours_list"
            equi_out.write(line + "\n")
            count = 0
            for group in self.equivalence:
                line = ""
                if len(group) > 1:
                    count += 1
                    # add group size
                    line += str(len(group)) + "\t"
                    # add group_elts column
                    tmp = ""
                    for elt in group:
                        tmp += elt + ", "
                    tmp = tmp[:-2]
                    line += tmp + "\t"
                    # add neighbour list size
                    line += str(len(self.node_neighbour[group[0]])) + "\t"
                    # add neighbours_list column
                    tmp = ""
                    for elt in self.node_neighbour[group[0]]:
                        tmp += elt + ", "
                    tmp = tmp[:-2]
                    line += tmp
                    equi_out.write(line + "\n")
                    number += 1
            equi_out.close()
        print("=> Writing equivalence file: done")
        print(f"Number of equivalence groups > 1: {count}")

    # Graph creation
    def graph_from_degree(self, d=None, bins=100, zero=False, log=False, ap=False):
        """
        :param d: dict containing the nodes and degree information
        :param bins: number of bins (sections)
        :param zero: does not take into account the value 1 if True
        :param log: logarithm scale for y axis
        :param ap: articulation point
        Create a distribution histogram of the degrees.
        """
        if self.node_neighbour == {} or self.node_degree == {}:
            self.set_neighbour_degree()
        color = "green"
        if d is None:
            dat = self.node_degree
        else:
            dat = d
        data_list = get_values_from_dict(dat)
        title = "Degree distribution"
        if ap:
            title += " of the articulation points"
            color = "deepskyblue"
            data_list = [x[0] for x in data_list]
        if zero:
            data_list = [x for x in data_list if x != 1]
            title += " - without degree = 1"
        num_bins = bins
        x = data_list
        plt.hist(x, num_bins, edgecolor='black', facecolor=color, alpha=0.5)
        # log scale
        if log:
            plt.yscale('log')
            title += " - log scale"
        plt.xlabel("Degree")
        plt.ylabel("Count")
        plt.title(title)
        plt.grid(True)

        plt.show()

    def graph_from_coef(self, d=None, bins=100, zero=False, log=False):
        """
        :param d: dict containing the nodes and degree information
        :param bins: number of bins (sections)
        :param zero: remove zero from list if True
        :param log: logarithm scale for y axis
        Create a distribution histogram of the local clustering coefficients.
        """
        if self.node_coef == {}:
            self.set_local_coef()
        if d is None:
            dat = self.node_coef
        else:
            dat = d
        data_list = get_values_from_dict(dat)
        title = "Local clustering coefficient distribution"
        if zero:
            data_list = [x for x in data_list if x != 0]
            title += " - without zero"
        num_bins = bins
        x = data_list
        plt.hist(x, num_bins, edgecolor='black', facecolor='blue', alpha=0.5)
        # log scale
        if log:
            plt.yscale('log')
            title += " - log scale"
        # to have the size of each bin
        # print(n)
        plt.xlabel("Coef")
        plt.ylabel("Count")
        plt.title(title)
        plt.grid(True)

        plt.show()

    def graph_from_children(self, bins=100, log=False):
        data_list = get_values_from_dict(self.ap)
        data_list = [x[1] for x in data_list]

        color = "orangered"
        title = "Distribution of articulation points' number of associated connected components"
        num_bins = bins
        x = data_list

        plt.hist(x, num_bins, edgecolor='black', facecolor=color, alpha=0.5)

        if log:
            plt.yscale('log')
            title += " - log scale"

        plt.xlabel("number of associated connected components")
        plt.ylabel("Count")
        plt.title(title)
        plt.grid(True)

        plt.show()

    # TODO add paramaters to chose the values of each categories + chose the colors
    def graph_from_coef_stacked(self, bins=100, zero=False, log=False):
        """
        :param bins: number of bins (sections)
        :param zero: remove zero from list if False
        :param log: logarithm scale for y axis
        Create a distribution histogram of the local clustering coefficients.
        3 colors for each bar:
            - green: degree <= 10
            - yellow: 10 < degree <= 50
            - red: degree > 50
        """
        if self.node_coef == {}:
            self.set_local_coef()
        title = "Local clustering coefficient distribution"

        # choose the colors and the different thresholds
        limit_list = [4, 10, 30]
        colors = ['mediumseagreen', 'greenyellow', 'gold', 'orangered']
        assert len(colors) == len(limit_list) + 1, "Colors length is not ok"

        x = []
        for i in range(len(limit_list)+1):
            # retrieve the nodes for each degree category
            # first value
            if i == 0:
                node_degree = {k: v for (k, v) in self.node_degree.items() if v <= limit_list[i]}
            # last value
            elif i == len(limit_list):
                node_degree = {k: v for (k, v) in self.node_degree.items() if v > limit_list[i-1]}
            else:
                node_degree = {k: v for (k, v) in self.node_degree.items() if limit_list[i-1] < v
                               <= limit_list[i]}
            # retrieve the coef for the selected nodes
            node_coef = {k: v for (k, v) in self.node_coef.items() if k in node_degree}
            # retrieve the list of values from node_coef
            values = list(node_coef.values())
            if zero:
                values[:] = [x for x in values if x != 0]
            # array w/ 1 column
            values = np.transpose(np.array([values]))
            x.append(values)
        if zero:
            title += " - without zero"

        # set the legend
        labels = []
        for i in range(len(limit_list)):
            labels.append(f"degree <= {limit_list[i]}")
        # add last legend item
        labels.append(f"degree > {limit_list[-1]}")

        num_bins = bins
        plt.hist(x, num_bins, edgecolor='black', histtype='bar', stacked=True, color=colors,
                 label=labels)

        # change axis length
        ymax = plt.gca().get_ybound()
        ymax = ymax[1] * 1.1
        xmax = plt.gca().get_xbound()[1]
        plt.axis([0, xmax, 0, ymax])

        # log scale
        if log:
            plt.yscale('log')
            title += " - log scale"

        # axis labels and title
        plt.xlabel("Coef")
        plt.ylabel("Count")
        plt.title(title)
        plt.legend(prop={'size': 10})
        plt.grid(True)

        plt.show()

    def graph_scatter(self):
        """
        Create a scatter plot. The point color changes with the density, the densest points are
        on top.
        x: degree
        y: 1 - (coef w/o zero)
        """
        if self.node_coef == {}:
            self.set_local_coef()
        # retrieve the wanted data
        coef_no_zero = {k: v for (k, v) in self.node_coef.items() if v != 0}
        degree = {k: v for (k, v) in self.node_degree.items() if k in coef_no_zero}

        # uncomment lines for the wanted representation (coef or 1-coef):
        coef_final = {k: (1 - v) for (k, v) in coef_no_zero.items()}
        ylab = "1 - Coef"
        # coef_final = {k: v for (k, v) in coef_no_zero.items()}
        # ylab = "Coef"

        list_degree = []
        list_coef = []
        for node in coef_final:
            list_degree.append(degree[node])
            list_coef.append(coef_final[node])
        x = np.array(list_degree)
        y = np.array(list_coef)

        # Calculate the point density
        xy = np.vstack([x, y])
        z = gaussian_kde(xy)(xy)

        # Sort the points by density, so that the densest points are plotted last
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

        fig, ax = plt.subplots()
        ax.scatter(x, y, c=z, s=50, edgecolor='', cmap='inferno')

        plt.xlabel("Degree")
        plt.ylabel(ylab)
        plt.title("Scatterplot of the degree and the clustering coefficient")

        plt.show()

    # Selection and creation of a sub-tabulated file w/ nodes w/ chosen coef
    def set_big_dict(self):
        """
        Create a dict containing for each node: its degree, its clustering coefficient and a list
        of its neighbours
        """
        print("=> Setting big dict...")
        big_dict = {}
        for node in self.node_coef:
            deg = self.node_degree[node]
            coef = self.node_coef[node]
            neig = self.node_neighbour[node]
            big_dict[node] = [deg, coef, neig]
        self.big_dict = big_dict
        print("=> Setting big dict: done")

    def select_coef(self, coef1, coef2=None):
        """
        :param coef1: inferior coef limit
        :param coef2: superior coef limit
        :return: a subdict of big_dict containing the nodes with a coef equal to coef 1,
        or a coef between coef1 and coef2
        """
        small_dict = {}
        if coef2 is None:
            for node in self.big_dict:
                coef = self.big_dict[node][1]
                if coef == coef1:
                    small_dict[node] = self.big_dict[node]
        else:
            assert coef1 <= coef2, "coef1 is not smaller that coef2"
            for node in self.big_dict:
                coef = self.big_dict[node][1]
                if coef1 <= coef <= coef2:
                    small_dict[node] = self.big_dict[node]
        return small_dict

    def select_protein(self, coef1, coef2=None):
        """
        :param coef1: inferior coef limit
        :param coef2: superior coef limit
        :return: list of selected proteins with a chosen coef
        """
        select_dict = self.select_coef(coef1, coef2)
        selected_prot = []
        for key in select_dict:
            selected_prot.append(key)
        return selected_prot

    def select_tab_coef(self, coef1, coef2=None, neighbour=False):
        """
        :param coef1: inferior coef limit
        :param coef2: superior coef limit
        :param neighbour: do not take into account the neighbour nodes if False
        :return: tabulated file containing the interaction lines with at least one node with a
        chosen coef
        """
        selected_protein = self.select_protein(coef1, coef2)
        selected_tab = []
        if neighbour:       # keep the neighbour node + nodes w/ chosen coef
            for i in range(len(self.tab_file)):
                if self.tab_file[i][1] in selected_protein or self.tab_file[i][2] in \
                        selected_protein:
                    selected_tab.append(self.tab_file[i])
        else:               # only keep interactions of nodes w/ chosen coef
            for i in range(len(self.tab_file)):
                if self.tab_file[i][1] in selected_protein and self.tab_file[i][2] in \
                        selected_protein:
                    selected_tab.append(self.tab_file[i])
        return selected_tab

    def write_tab_coef(self, coef1, coef2=None, neighbour=False):
        """
        :param coef1: inferior coef limit
        :param coef2: superior coef limit
        :param neighbour: do not take into account the neighbour nodes if False
        :return: asp filename and tabulated filename
        create a tabulated file and an asp file in the result folder
        """
        print("=> Writing tabulated and ASP files...")
        tab = self.select_tab_coef(coef1, coef2, neighbour)
        # define the file names
        base_filename = re.sub("_tab", "", self.base_filename)
        if coef2 is None:   # only 1 coef
            if neighbour:
                tab_filename = f"{self.result_folder}{base_filename}_tab_{coef1}_neighbour.csv"
                asp_filename = f"{self.result_folder}{base_filename}_{coef1}_neighbour.lp"
            else:
                tab_filename = f"{self.result_folder}{base_filename}_tab_{coef1}.csv"
                asp_filename = f"{self.result_folder}{base_filename}_{coef1}.lp"
        else:   # 2 coefs
            if neighbour:
                tab_filename = f"{self.result_folder}{base_filename}_tab_{coef1}-" \
                               f"{coef2}_neighbour.csv"
                asp_filename = f"{self.result_folder}{base_filename}_{coef1}-{coef2}_neighbour.lp"
            else:
                tab_filename = f"{self.result_folder}{base_filename}_tab_{coef1}-{coef2}.csv"
                asp_filename = f"{self.result_folder}{base_filename}_{coef1}-{coef2}.lp"
        with open(tab_filename, "w", encoding='UTF-8') as tab_out:
            with open(asp_filename, "w") as asp_out:
                for i in range(len(tab)):
                    # write in the tabulated file
                    writing = ""
                    for j in range(len(tab[i])):
                        writing += str(tab[i][j]) + "\t"
                    tab_out.write(writing[:-1] + "\n")
                    # write in asp file
                    asp_line = 'edge("' + tab[i][1] + '","' + tab[i][2] + '").'
                    asp_out.write(asp_line + "\n")
                asp_out.close()
            tab_out.close()
        print("=> Writing tabulated and ASP files: done")
        return asp_filename, tab_filename

    def write_cyto_coef(self):
        """
        Write a tabulated file containing, for each node, its clustering coefficient. Can be used
        in cytoscape for visualization.
        """
        self.set_neighbour_degree()
        self.set_local_coef()
        print("=> Writing csv file for Cytoscape...")
        cyto_filename = f"{self.result_folder}{self.base_filename}_cytoscape.csv"
        with open(cyto_filename, 'w') as cyto_out:
            for node in sorted(self.node_coef):
                line = str(node) + "\t" + str(self.node_coef[node])
                cyto_out.write(line + "\n")
            cyto_out.close()
        print("=> Writing csv file for Cytoscape: done")

    # Selection and elimination of hub nodes
    def select_hub(self, hub: float):
        """
        :param hub: threshold for the hub (ex: hub = 0.1 if you want the hubs to be the 1% most
        connected node)
        :return: a list of tuples (node, degree) of the nodes considered as hubs
        """
        self.set_neighbour_degree()
        # list of tuple (node, degree)
        count_list = self.node_degree.items()
        # sort the list by descending degree value
        sorted_count_list = sorted(count_list, reverse=True, key=lambda x: x[1])
        # number of hubs
        nb_hub = int(round(self.nnode * hub))
        # nodes w/ the higher degree
        chosen_nodes = sorted_count_list[:nb_hub]
        return chosen_nodes

    def remove_hub(self, hub: float):
        """
        :param hub: threshold for the hub (ex: hub = 0.1 if you want the hubs to be the 1% most
        connected node)
        :return: a tabulated file without the hub lines
        Remove the hubs from the tab_file
        """
        print("=> Removing hubs...")
        chosen_nodes = self.select_hub(hub)
        final_data = self.tab_file
        removed_hub = []
        for node_tuple in chosen_nodes:
            node_name = node_tuple[0]
            removed_hub.append(node_name)
            # remove the lines that contain the hub nodes
            final_data[:] = [x for x in final_data if x[1] != node_name and x[2] != node_name]
        print("=> Removing hubs: done")
        hub_str = ""
        for h in sorted(removed_hub):
            hub_str += h + ", "
        hub_str = hub_str[:-2]
        print(f"Removed {len(removed_hub)} hubs: {hub_str}")
        return final_data

    def write_tab_hub(self, hub: float):
        """
        :param hub: threshold for the hub (ex: hub = 0.1 if you want the hubs to be the 1% most
        connected node)
        Write a tabulated file and an asp file without the hubs
        """
        tab = sorted(self.remove_hub(hub))
        print("=> Writing tabulated file...")
        # choose output file names
        tab_filename = f"{self.result_folder}{self.base_filename}_no_hub_{hub}.csv"
        asp_filename = f"{self.result_folder}{self.base_filename}_no_hub_{hub}.lp"
        asp_filename = re.sub("_tab", "", asp_filename)
        # write in output files
        with open(tab_filename, "w", encoding='UTF-8') as tab_out:
            with open(asp_filename, "w") as asp_out:
                for i in range(len(tab)):
                    # write in the tabulated file
                    writing = ""
                    for j in range(len(tab[i])):
                        writing += str(tab[i][j]) + "\t"
                    tab_out.write(writing[:-1] + "\n")
                    # write in asp file
                    asp_line = 'edge("' + tab[i][1] + '","' + tab[i][2] + '").'
                    asp_out.write(asp_line + "\n")
                asp_out.close()
            tab_out.close()
        print("=> Writing tabulated file: done")

    def write_cyto_hub(self, hub: float):
        """
        :param hub:threshold for the hub (ex: hub = 0.1 if you want the hubs to be the 1% most
        connected node)
        Write a csv file for each hub with the list of the connected nodes. Can be used in
        cytoscape to set up tags.
        """
        # folder name
        base = os.path.basename(self.infile)                    # file name without the folder path
        filename_prefix = os.path.splitext(base)[0]             # file name without the extension
        hub_folder = f"{self.result_folder}{filename_prefix}_hub_{hub}/"
        # create folder
        if not os.path.exists(hub_folder):
            os.makedirs(hub_folder)

        # tagging the nodes
        it = 1
        chosen_nodes = self.select_hub(hub)
        print("=> Writing tabulated files...")
        for node in chosen_nodes:
            # tabulated file for the hub
            tab = []
            hub_name = node[0]
            # nodes connected to the hub
            neighbour_list = self.node_neighbour[hub_name]
            # the neighbours are tagged with the hub name
            for prot in neighbour_list:
                tab_line = [prot, hub_name]
                tab.append(tab_line)

            # file name
            filename = f"{hub_folder}{filename_prefix}_hub_{hub}_{hub_name}.csv"

            # writing the tabulated file
            with open(filename, 'w') as tab_out:
                for i in range(len(tab)):
                    writing = ""
                    for j in range(len(tab[i])):
                        writing += str(tab[i][j]) + "\t"
                    tab_out.write(writing[:-1] + "\n")
                tab_out.close()
            print(f"=> Writing tabulated files: {it} file(s) done")
            it += 1
        print("=> Writing tabulated files: all done")

    # Selection of nodes to produce compressed graphs of different size
    def select_node_percent(self, node_percent: float):
        """
        :param node_percent: threshold, percentage of nodes kept for the creation of the new
        tabulated file (ex: 0.1 for 10 % of the nodes)
        :return: a list of the selected nodes
        Keep the least connected nodes
        """
        self.set_neighbour_degree()
        # list of tuple (node, degree)
        count_list = self.node_degree.items()
        # sort the list by ascending degree value
        sorted_count_list = sorted(count_list, reverse=False, key=lambda x: x[1])
        # number of selected nodes
        nb_node = int(round(self.nnode * node_percent))
        # nodes w/ the smaller degree
        chosen_nodes = sorted_count_list[:nb_node]
        # list of nodes
        chosen_nodes[:] = [x[0] for x in chosen_nodes]
        # list of tuple (id, node name)
        chosen_dict = {ident: node_name for (ident, node_name) in self.id_name.items() if
                       node_name in chosen_nodes}
        print("Number of selected nodes: ", len(chosen_nodes))
        return chosen_nodes, chosen_dict

    def select_tab_node_percent(self, node_percent: float):
        """
        :param node_percent: threshold, percentage of nodes kept for the creation of the new
        tabulated file (ex: 0.1 for 10 % of the nodes)
        :return: a tabulated file w/ the induced graph of the selected nodes + tuple (number of
        chosen nodes, number of interactions) + the list of the selected nodes
        """
        chosen_nodes, chosen_dict = self.select_node_percent(node_percent)
        final_data = [x for x in self.tab_file if x[1] in chosen_nodes and x[2] in chosen_nodes]
        print("Number of interactions: ", len(final_data))
        return final_data, (len(chosen_nodes), len(final_data)), chosen_dict

    def write_tab_node_percent(self, node_percent: float):
        """
        :param node_percent: threshold, percentage of nodes kept for the creation of the new
        tabulated file (ex: 0.1 for 10 % of the nodes)
        :return: the created tabulated filename + tuple (number of chosen nodes, number of
        interactions)
        Write a tabulates file and an asp file with the selected nodes.
        """
        assert node_percent <= 1, "node_percent is not under 1."
        selected_tab = self.select_tab_node_percent(node_percent)
        tab = selected_tab[0]
        tup = selected_tab[1]
        chosen_dict = selected_tab[2]
        chosen_dict = collections.OrderedDict(sorted(chosen_dict.items()))

        print("=> Writing tabulated file...")

        # choose output file names
        perc = node_percent * 100
        if not os.path.exists(f"{self.result_folder}/percent/"):
            os.mkdir(f"{self.result_folder}/percent/")
        tab_filename = f"{self.result_folder}/percent/{self.base_filename}_{perc}_percent.csv"
        chosen_dict_filename = f"{self.result_folder}/percent/{self.base_filename}_" \
                               f"{perc}_percent_all_node_name.csv"
        chosen_dict_filename = re.sub("_tab", "", chosen_dict_filename)
        asp_filename = f"{self.result_folder}/percent/{self.base_filename}_{perc}_percent.lp"
        asp_filename = re.sub("_tab", "", asp_filename)

        # write in output files
        with open(tab_filename, "w", encoding='UTF-8') as tab_out:
            with open(asp_filename, "w") as asp_out:
                for i in range(len(tab)):
                    # write in the tabulated file
                    writing = ""
                    for j in range(len(tab[i])):
                        writing += str(tab[i][j]) + "\t"
                    tab_out.write(writing[:-1] + "\n")
                    # write in asp file
                    asp_line = 'edge("' + tab[i][1] + '","' + tab[i][2] + '").'
                    asp_out.write(asp_line + "\n")
                asp_out.close()
            tab_out.close()

        write_csv_from_ordered_dict(chosen_dict, chosen_dict_filename)
        print("=> Writing tabulated file: done")
        return tab_filename, tup

    # Selection of isoforms
    def set_iso_tab_file(self):
        """
        Keep the lines with isoforms and set the isoform tab file
        """
        new_tab = self.tab_file[:]
        new_tab[:] = [x for x in new_tab if ("-" in x[3] and "EBI-" not in x[3]) or ("-" in x[4]
                      and "EBI-" not in x[4])]
        self.iso_tab_file = new_tab

    def set_isoforms(self):
        """
        Set the isoform list, a list of tuple for the isoforms (alias, id) (xxxxx-nn)
        """
        self.set_iso_tab_file()
        print("=> Setting isoforms...")
        iso_list = []
        # add isoform id
        for i in range(len(self.iso_tab_file)):
            for j in range(3, 5):
                ident = self.iso_tab_file[i][j]
                alias = self.iso_tab_file[i][j-2]
                if "-" in ident and (alias, ident) not in iso_list and "EBI-" not in ident:
                    iso_list.append((alias, ident))
        iso_list = sorted(iso_list)
        print("Number of isoforms: ", len(iso_list))
        # add original id
        iso_list_copy = iso_list
        for tup in iso_list_copy:
            alias = tup[0]
            for i in range(len(self.tab_file)):
                for j in range(1, 3):
                    alias_test = self.tab_file[i][j]
                    id_test = self.tab_file[i][j+2]
                    tuple_test = (alias_test, id_test)
                    if alias_test == alias and tuple_test not in iso_list:
                        iso_list.append(tuple_test)
        self.iso_list = sorted(iso_list)
        print("=> Setting isoforms: done")

    def write_isoforms(self):
        """
        Write the isoform tuple list into a tabulated file
        """
        self.set_isoforms()

        # folder and file name
        base = os.path.basename(self.infile)                    # file name without the folder path
        filename_prefix = os.path.splitext(base)[0]             # file name without the extension
        filename = f"{self.result_folder}{filename_prefix}_isoforms.csv"

        # writing file
        print("=> Writing isoforms...")
        with open(filename, 'w') as tab_out:
            writing = "alias\tid"
            tab_out.write(writing + "\n")
            for tup in self.iso_list:
                writing = f"{tup[0]}\t{tup[1]}"
                tab_out.write(writing + "\n")
            tab_out.close()
        print("=> Writing isoforms: done")

    # Tree detection
    def is_connected(self):
        """
        :return: True if the subgraph is a connected graph
        """
        self.set_neighbour_degree()
        self.set_connected_comp()
        return self.nb_connected_component == 1

    def has_n_minus_one_edges(self):
        """
        :return: True if the subgraph has n nodes and (n-1) edges
        """
        return self.nedge == self.nnode - 1

    def is_tree(self):
        """
        :return: True if is a tree (is a connected graph AND has (n-1) edges)
        """
        return self.is_connected() and self.has_n_minus_one_edges()

    def remove_tree(self, tree_folder):
        """
        :param tree_folder: directory path containing the trees
        Remove the trees from the graph and write the new tabulated file + asp file
        """
        print("Removing trees...")
        # remove the lines of the tree(s)
        new_tab_file = self.tab_file
        file_list = os.listdir(tree_folder)
        for f in file_list:
            file = open_data(tree_folder + f)
            for line in file:
                new_tab_file[:] = [x for x in new_tab_file if x != line]
        # choose output file names
        base = os.path.basename(self.tab_filename)      # file name without the folder path
        filename_prefix = os.path.splitext(base)[0]     # file name without the extension
        base = os.path.basename(self.infile)
        folder_prefix = os.path.splitext(base)[0]
        folder_name = "./" + folder_prefix + "/"        # folder containing the result file
        tab_filename = folder_name + filename_prefix + "_no_tree.csv"
        asp_filename = folder_name + filename_prefix + "_no_tree.lp"
        asp_filename = re.sub("_tab", "", asp_filename)
        # write in output files
        with open(tab_filename, "w", encoding='UTF-8') as tab_out:
            with open(asp_filename, "w") as asp_out:
                for i in range(len(new_tab_file)):
                    # write in the tabulated file
                    writing = ""
                    for j in range(len(new_tab_file[i])):
                        writing += str(new_tab_file[i][j]) + "\t"
                    tab_out.write(writing[:-1] + "\n")
                    # write in asp file
                    asp_line = 'edge("' + new_tab_file[i][1] + '","' + new_tab_file[i][2] + '").'
                    asp_out.write(asp_line + "\n")
                asp_out.close()
            tab_out.close()
        print("Removing trees: done")

    # Articulation point detection
    def ap_util(self, u, visited, ap, parent, low, disc):
        """
        Adapted from:
        https://www.geeksforgeeks.org/articulation-points-or-cut-vertices-in-a-graph/

        A recursive function that find articulation points using DFS traversal
        :param u: the vertex to be visited next
        :param ap: stores articulation points
        :param visited: keeps tract of visited vertices
        :param parent: stores parent vertices in DFS tree
        :param low: stores the low time of visited vertices
        :param disc: stores discovery times of visited vertices
        """
        # Count of children in current node
        children = 0

        # Mark the current node as visited and print it
        visited[u] = True

        # Initialize discovery time and low value
        disc[u] = self.time
        low[u] = self.time
        self.time += 1

        # Recursion call for all the vertices adjacent to this vertex
        for v in self.node_neighbour[u]:
            # If v is not visited yet, then make it a child of u in DFS tree and recur for it
            if visited[v] is False:
                parent[v] = u
                children += 1
                v, visited, ap, parent, low, disc = self.ap_util(v, visited, ap, parent, low, disc)

                # Check if the subtree rooted with v has a connection to one of the ancestors of u
                low[u] = min(low[u], low[v])

                # u is an articulation point in following cases:
                #  (1) u is root of DFS tree and has two or more children
                if parent[u] is None and children > 1:
                    ap[u] = True

                #  (2) If u is not root and low value of one of its child is more or equal to
                # discovery value of u
                if parent[u] is not None and low[v] >= disc[u]:
                    ap[u] = True

            # Update low value of u for the parent function calls
            elif v != parent[u]:
                low[u] = min(low[u], disc[v])
        if parent[u] is not None:
            self.children[u] = children + 1
        else:
            self.children[u] = children
        return u, visited, ap, parent, low, disc

    def set_ap(self):
        """
        The function to do DFS traversal. It uses recursive ap_util()
        """
        def default_disc():
            """
            :return: default value for disc and low dict
            """
            return float("Inf")

        def default_parent():
            """
            :return: default value for the parent dict
            """
            return None

        if self.node_neighbour == {}:
            self.set_neighbour_degree()
        print("=> Searching for articulation points...")
        # Set the default values of each dict, default for all the vertices is not visited
        # Initialize parent of the visited vertices, and ap (articulation point) dict
        visited = defaultdict(bool)
        disc = defaultdict(default_disc)
        low = defaultdict(default_disc)
        parent = defaultdict(default_parent)
        ap = defaultdict(bool)  # To store articulation points

        # Call the recursive helper function to find articulation points in DFS tree rooted with
        # vertex 'i'
        for i in self.node_list:
            if visited[i] is False:
                i, visited, ap, parent, low, disc = self.ap_util(i, visited, ap, parent, low, disc)

        for node, value in ap.items():
            if value is True:
                deg = self.node_degree[node]
                # number of connected components linked to the articulation point
                con_comp_ap = self.children[node]
                self.ap[node] = (deg, con_comp_ap)

        # Histogram representations
        self.graph_from_degree(d=self.ap, ap=True, log=True)
        self.graph_from_children(log=True)
        print("=> Searching for articulation points: done")

    def write_ap(self):
        """
        Write a tabulated file containing for each articulation point, its degree and the number
        of associated connected components.
        """
        if self.ap == {}:
            self.set_ap()
        print("=> Writing tabulated file...")
        # TODO ratio nb connected comp / degree
        filename_prefix = self.base_filename
        filename_prefix = re.sub("_tab", "", filename_prefix)
        ap_filename = f"{self.result_folder}{filename_prefix}_articulation_points.csv"
        with open(ap_filename, 'w') as ap_out:
            line = "Articulation points\tDegree\tAssociated connected components\tRatio " \
                   "connected component / degree"
            ap_out.write(f"{line}\n")
            for k, v in self.ap.items():
                degree = v[0]
                conn_comp = v[1]
                ratio = round(v[1] / v[0], 3)
                line = f"{k}\t{degree}\t{conn_comp}\t{ratio}"
                ap_out.write(f"{line}\n")
            ap_out.close()
        print("=> Writing tabulated file: done")

    # Print general info about the graph
    def print_stat(self):
        """
        print general info about the graph
        """
        print(f"The graph contains {self.nnode} nodes and {self.nedge} edges.")
        print(f"The global clustering coefficient is: {self.density}")
