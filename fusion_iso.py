import argparse
import csv
import os
import re

def open_data(file: str):
    """
    :param file: file path
    :return: a readable file for the program
    """
    with open(file, "r") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        return list(reader)

parser = argparse.ArgumentParser()
parser.add_argument("infile", type=str, help="csv input file", metavar='INFILE')
parser.add_argument("correspondance", type=str, help="correspondance file")
args = parser.parse_args()


file = open_data(args.infile)
corresp = open_data(args.correspondance)

filename = os.path.splitext(args.infile)[0]
filename_csv = f"{filename}_bis.csv"
filename_lp = f"{filename}_bis.lp"

#filter
for i in range(len(file)):
    for j in range(len(file[i])):
        for k in range(len(corresp)):
            if file[i][j] == corresp[k][0]:
                file[i][j] = corresp[k][1]

file[:] = [x for x in file if x[1] != "-" or x[2] != "-"]   # doesn't work...

with open(filename_csv, "w", encoding='UTF-8') as outcsv:
    writer = csv.writer(outcsv, delimiter='\t')
    with open(filename_lp, "w", encoding='UTF-8') as outlp:
        for line in file:
            writer.writerow(line)
            asp_line = f'edge("{line[1]}","{line[2]}").\n'
            outlp.write(asp_line)
        outlp.close()
    outcsv.close()

