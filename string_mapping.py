#!/usr/bin/env python

###############################################################################
# For a given list of proteins the script resolves them (if possible) to the
# best matching STRING identifier and prints out the mapping on screen in
# the TSV format
###############################################################################

import json
import requests
import sys


def string_mapping(protein_list: list,
                   out_format: str = "tsv-no-header",
                   method: str = "get_string_ids",
                   species: str = "9606") -> tuple:
    string_api_url = "https://string-db.org/api"
    output_format = out_format
    method = method

    my_genes = protein_list
    species = species           # 9606 = homo sapiens
    limit = 1                   # limits the number of matches per query identifier (best matches
                                # come first)
    echo_query = "1"            # insert column with your input identifier (takes values '0' or '1',
                                # default is '0')

    # Construct the request
    request_url = f"{string_api_url}/{output_format}/{method}?"
    request_url += f"identifiers={'%0d'.join(my_genes)}"
    request_url += f"&species={species}"
    request_url += f"&limit={str(limit)}"
    request_url += f"&echo_query={echo_query}"

    # Call STRING
    try:
        r = requests.get(request_url)
    except requests.exceptions.RequestException as err:
        print(err)
        sys.exit(1)

    # Read and parse the results
    rfile = r.text.split("\n")

    string_id_list = []     # identifiers used by STRING
    initial_id_list = []    # common names
    print("\n\tInput\tAlias\tSTRING ID")
    for line in rfile:
        l = line.split("\t")
        #print(l)
        if len(l) > 1:
            input_identifier, string_identifier, string_alias = l[0], l[2], l[5]
            print(f"\t{input_identifier}\t{string_alias}\t{string_identifier}")
            string_id_list.append(string_identifier)
            initial_id_list.append(string_alias)
    print("")

    return string_id_list, initial_id_list

if __name__ == '__main__':
    print(string_mapping(["YMR055C", "YFR028C"], species="4932"))
