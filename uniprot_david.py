#!/usr/bin/python
#-*- coding: utf-8 -*-

import csv
import os


def writing_file(name, list_data):
    with open(name, "w", encoding='UTF-8') as ddl:
        for i in range(len(list_data)):
            ddl.write(str(list_data[i]) + "\n")
    ddl.close()


def list_id(pwrn_file, ref_file):
    """
    :param pwrn_file: File containing the powernodes
    :param ref_file: File containing the Uniprot reference identifiers
    :return: Uniprot IDs list for DAVID annotation
    """
    # Checking input arguments
    assert pwrn_file[-4:] == '.csv', "The powernode file extension is not '.csv'"
    assert ref_file[-4:] == '.csv', "The ref file extension is not '.csv'"
    assert os.path.isfile(pwrn_file), "uniprot_david: The powernode file does not exist!"
    assert os.path.isfile(ref_file), "uniprot_david: The file with the Uniprot reference " \
                                     "identifiers does not exist!"

    # Create the folder
    folder = pwrn_file[:-4]
    i = 1
    while os.path.exists(folder):
        if i == 1:
            folder += "_1"
        else:
            folder = folder[:-2] + "_" + str(i)
        i += 1
    os.makedirs(folder)

    # Retrieve data
    def open_data(name):
        with open(name, "r") as df:
            reader = csv.reader(df, delimiter="\t")
            return list(reader)
    data_pwrn = open_data(pwrn_file)
    data_ref = open_data(ref_file)

    # Create file for writing
    dataset = []
    for i in range(len(data_pwrn)):
        for k in range(len(data_ref)):
            donnee = []
            if data_ref[k][1] == data_pwrn[i][0]:
                donnee = [data_ref[k][1], data_ref[k][3], data_ref[k][5]]
            elif data_ref[k][2] == data_pwrn[i][0]:
                donnee = [data_ref[k][2], data_ref[k][4], data_ref[k][6]]
            if (donnee not in dataset) and donnee != []:
                dataset.append(donnee)

    # Remove duplicates
    deleted_data = []
    for i in range(len(dataset)):
        for j in range(len(dataset)):
            if dataset[i][0] == dataset[j][0] and dataset[i][2] == dataset[j][2] and i != j:
                if ((dataset[i][1][-2] == "-" and dataset[i][1][:-2] == dataset[j][1])
                        or (dataset[i][1][-3] == "-" and dataset[i][1][:-3] == dataset[j][1])):
                    if dataset[i] not in deleted_data:
                        deleted_data.append(dataset[i])
                elif ((dataset[j][1][-2] == "-" and dataset[j][1][:-2] == dataset[i][1])
                        or (dataset[j][1][-3] == "-" and dataset[j][1][:-3] == dataset[i][1])):
                    if dataset[j] not in deleted_data:
                        deleted_data.append(dataset[j])
    for k in range(len(deleted_data)):
        dataset.remove(deleted_data[k])

    # Search the name of powernode
    i = len(pwrn_file) - 1
    while pwrn_file[i-4:i] != 'PWRN' and i >= 0:
        i -= 1
    pwrn = str(pwrn_file[i+1:-4])

    # Create file containing a list of the molecules in the powernode
    filename = folder + "/list_" + pwrn + ".txt"
    with open(filename, "w", encoding='UTF-8') as ddl:
        id_num = 0
        for i in range(len(dataset)):
            writing = ""
            for j in range(len(dataset[i])):
                writing += str(dataset[i][j]) + "\t"
            ddl.write(writing[:-1] + "\n")
            id_num += 1
    ddl.close()

    # Create Folder
    # TODO change outputname_init, outputname_final --> output_name ??
    outputname_init = folder + "/" + pwrn + "_listinit.txt"
    with open(outputname_init, "w", encoding='UTF-8') as ddl:
        for i in range(len(dataset)):
            ddl.write(str(dataset[i][1]) + "\n")
    ddl.close()

    # Create list of the molecule id in the reference file:
    assert os.path.isdir('listing'), "The 'listing' folder does not exist!"
    assert os.path.isfile("listing/list_id.csv"), "The 'list_id.csv' file does not exist!"
    data_id = open_data("listing/list_id.csv")
    deleted_data = []
    datafile = []
    for i in range(len(dataset)):
        check = False
        for j in range(len(data_id)):
            if (dataset[i][1] == data_id[j][0] 
                    and (dataset[i][2] == data_id[j][2] or dataset[i][2] == 'unknown')):
                dataset[i][1] = data_id[j][1]
        if dataset[i][1][:4] == "ebi:":
            for j in range(len(data_id)):
                if dataset[i][1][4:] == data_id[j][0] \
                        and (dataset[i][2] == data_id[j][2] or dataset[i][2] == 'unknown'):
                    check = True
                    if data_id[j][1] not in datafile:
                        datafile.append(data_id[j][1])
        elif dataset[i][1][:6] == "PFRAG_" or dataset[i][1][:6] == "chebi:":
            check = False
        else:
            check = True
            pro = False
            for k in range(len(dataset[i][1]) - 5):
                if dataset[i][1][k:k+5] == "-PRO_":
                    if dataset[i][1][:k] not in datafile:
                        datafile.append(dataset[i][1][:k])
                    pro = True
            if pro is False and (dataset[i][1] not in datafile):
                datafile.append(dataset[i][1])
        if check is False:
            deleted_data.append(dataset[i][1])

    # Create a file with the molecules not included in the final file
    if len(deleted_data) > 0:
        outputname_del = outputname_init[:-13] + "_listdelete.txt"
        writing_file(outputname_del, deleted_data)

    # Create final file with Uniprot identifiers
    outputname_final = outputname_init[:-13] + ".txt"
    writing_file(outputname_final, datafile)
    return datafile, outputname_final, deleted_data
