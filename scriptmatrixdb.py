#!/usr/bin/python
#-*- coding: utf-8 -*-

"""
input : .mitab file
output : .bbl file

The .mitab file is first cleaned and converted into a .csv file via the clean_mitab.py script.
The .csv file is then converted into ASP language (.lp file) via the converter_csv_asp.py script.
Lastly, a .bbl file is created. It can be visualized with the Cytoscape software or it can be
taken as an input for the bubbletools software, which chooses the interesting powernodes and
annotates them with the DAVID annotation tools.
"""
import argparse
import os
import csv
import time
import clean_mitab
import converter_csv_asp
import bblINcsv
import choice_node_bbl

start = time.time()


def open_data(file: str):
    with open(file, "r") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        return list(reader)


# Parser:
parser = argparse.ArgumentParser()
parser.add_argument("infile", type=str, help=".mitab input file", metavar='INFILE')
parser.add_argument("--A", type=int, help="The column number of the alias A (positive number).",
                    metavar='col_number_alias_A', default=5)
parser.add_argument("--B", type=int, help="The column number of the alias B (positive number).",
                    metavar='col_number_alias_B', default=6)
parser.add_argument("--graph", choices=['powergraph', 'oriented'], default='powergraph',
                    type=str, help="Powergraph or oriented graph.")
parser.add_argument("--interac", type=str, default='none',
                    choices=['human', 'human-mouse', 'chicken', 'mouse', 'mouse-rat', 'dog',
                             'taurus', 'rat', 'sheep', 'pig', 'none', 'unknown'],
                    help="Taxon filter for the graph compression.")
parser.add_argument("--render", action="store_true", help="Generate a png image with the "
                                                          "powergraph plugin (Oog) on Cytoscape.")
parser.add_argument("--annot", type=str, help="csv file for DAVID ", metavar='Annotation',
                    default="annot/annot.csv")
parser.add_argument("--allpwrn", help="To have the maximum concept (all powernodes).",
                    action="store_true")
parser.add_argument("--score", type=float, help="Minimum enrichment score (positive number).",
                    default=2)
parser.add_argument("--pv", type=float, help="p-value for functional annotation (positive decimal "
                    "number between 0 and 1).", metavar='pvalue', default=0.05)
parser.add_argument("--withoutCHEBI", action="store_true", help="Does not take into account the "
                    "molecules with ChEBI identification (non-protein molecules).")
parser.add_argument("--graphonly", action="store_true", help="To have only the powergraph.")
parser.add_argument("--aspfile", type=str, help="pre existing asp file from converter.")
parser.add_argument("--tabfile", type=str, help="pre existing tabulated file from converter.")
args = parser.parse_args()

# Checking input arguments:
assert args.infile[-6:] == '.mitab', "Infile extension is not '.mitab'"
assert os.path.isfile(args.infile), "Input file does not exist!"
assert os.path.isfile("annot/aliases.csv"), "annot/aliases.csv file does not exist!"
assert os.path.isfile("annot/taxon_aliases.csv"), "annot/taxon_aliases.csv file does not exist!"
assert os.path.isfile("annot/decomposables.csv"), "annot/decomposables.csv file does not exist!"
assert type(args.A) == int, "The col_number_alias_A argument for scriptmatrixdb is not an integer."
assert args.A > 0, "The col_number_alias_A argument is not a nonzero positive number."
assert type(args.B) == int, "The col_number_alias_B argument for scriptmatrixdb is not an integer."
assert args.B > 0, "The col_number_alias_B argument is not a nonzero positive number."
assert args.annot[-4:] == '.csv', "The annotation input file extension is not '.csv'."
assert os.path.isfile(args.annot), "The annotation file does not exist!"
assert args.score >= 0.0, "The enrichment score argument is not a positive number."
assert 0.0 <= args.pv <= 1.0, "The pvalue argument is not a positive decimal number between 0 " \
                              "and 1."


def graph_compression(asp_filename):
    print("=> Graph compression...")
    output_name = asp_filename[:-3] + ".bbl"
    os.system("powergrasp {} {}".format(asp_filename, output_name))
    assert os.path.isfile(output_name), "The .bbl output file from the powergrasp software does " \
                                        "not exist!"
    print("=> Graph compression: done")
    return output_name

def node_annotation(output_bbl):
    print("=> Functional annotation...")
    if args.allpwrn:  # to have the maximal concept
        node = choice_node_bbl.node(output_bbl, 'all')
    else:  # to have a random concept
        node = choice_node_bbl.node(output_bbl)
    for i in range(len(node)):
        for j in range(len(node[i][2])):
            assert os.path.isfile(node[i][2][j]), "The file " + node[i][2][
                j] + " does not exist!"
    bblINcsv.list_goterm(node, args.annot, args.score, args.pv, clean_files[1],
                         args.withoutCHEBI, converted_files[1])
    print("=> Functional annotation: done")


if not args.aspfile and not args.tabfile:
    # Cleaning the mitab file
    clean_files = clean_mitab.clean(args.infile, args.interac, args.A, args.B, args.withoutCHEBI)
    print("=> Homogenization and filtration of protein interactions: done")

    # Conversion of the csv file to ASP language
    assert os.path.isfile(clean_files[0]), "The cleaned mitab file (.csv) does not exist!"
    converted_files = converter_csv_asp.converter(clean_files[0])
    assert os.path.isfile(converted_files[0]), "The ASP file after conversion does not exist!"
    assert os.path.isfile(converted_files[1]), "The tabulated file after conversion  does not " \
                                               "exist!"
    print("=> Conversion to ASP: done")

    # Graph compression
    out_bbl = graph_compression(converted_files[0])

    # Visualization of visible relations of the powergraph:
    # TODO remplacer extension bbl par png (pour ne pas avoir file.bbl.png)
    # TODO ranger le png dans le meme dossier que les autres fichiers
    if args.render:
        os.system(
            'java -jar Oog_CommandLineTool2012/Oog.jar -inputfiles=' + out_bbl + ' -img -f=png')

    # Node Annotation:
    if not args.graphonly:
        node_annotation(out_bbl)
else:
    asp_file = open_data(args.aspfile)
    # tab_file = open_data(args.tabfile)
    # Graph compression
    out_bbl = graph_compression(args.aspfile)

    # Visualization of visible relations of the powergraph:
    # TODO remplacer extension bbl par png (pour ne pas avoir file.bbl.png)
    # TODO ranger le png dans le meme dossier que les autres fichiers
    if args.render:
        os.system(
            'java -jar Oog_CommandLineTool2012/Oog.jar -inputfiles=' + out_bbl + ' -img -f=png')

    # Node Annotation:
    if not args.graphonly:
        node_annotation(out_bbl)

end = time.time()
time_elapsed = end - start
print("Time elapsed: " + str(time.strftime('%H %M %S', time.gmtime(time_elapsed))))
