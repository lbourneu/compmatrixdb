#!/usr/bin/python
#-*- coding: utf-8 -*-
import csv
import re
import requests
import os
from bs4 import BeautifulSoup


def research(infile, annot_file, annot_type, list_pwrn_david):
    """

    :param infile:
    :param annot_file:
    :param annot_type:
    :param list_pwrn_david:
    :return:

    The function search for annotations in the DAVID API
    """
    # Checking input arguments
    assert type(infile) == str, "The infile not a string."
    assert infile.endswith('.csv') or infile.endswith('.txt'), "Input file extension is not " \
                                                               "'.csv' or '.txt' "
    assert os.path.isfile(infile), "search_david: infile does not exist!"
    assert type(annot_file) == str, "The annotation file for research is not string."
    assert annot_file.endswith('.csv'), "The annotation file extension is not '.csv' "
    assert os.path.isfile(annot_file), "search_david: the annotation file does not exist!"

    # Retrieve the input filename:
    i = len(infile) - 1
    while infile[i] != '/' and i >= 0:
        i -= 1
    pwrn_name = infile[i+1:-4]
    folder = infile[:i]

    # Open data
    def open_data(name):
        with open(name, "r") as data:
            reader = csv.reader(data, delimiter="\t")
            return list(reader)

    # List id of gene or protein
    datalist = open_data(infile)
    listid = ""
    for i in range(len(datalist)):
        listid += str(datalist[i][0]) + ","

    # Annotation list:
    dataannot = open_data(annot_file)
    listannot = ""
    for i in range(len(dataannot)):
        listannot += str(dataannot[i][0]) + ","

    # Function for API David request:
    def request_david(REQUEST, outhtml, output, name_PWRN, listPWRN):
        result = False
        BASE_URL = 'https://david.ncifcrf.gov/api.jsp?'
        INTERNAL_QUERY_URL = 'https://david.ncifcrf.gov/term2term.jsp'
        REG_FORM = re.compile(r'^document\.apiForm\.([^.]+)\.value="([^"]+)')
        # REG_FORM_ACTION = re.compile(r'^document\.apiForm\.action = "([^"]+)')
        if name_PWRN not in listPWRN:
            ####TEST SESSION####
            try:
                session = requests.Session()
            except:
                print("Connexion session problem.")
                print('Request on first page ({})'.format(BASE_URL + REQUEST))
            else:
                js_payload = session.get(BASE_URL + REQUEST).text

                def get_fields(payload: str):
                    for line in payload.splitlines():
                        match = REG_FORM.match(line)
                        if match:
                            yield match.groups()
                payload = dict(get_fields(js_payload))
                    ####TEST REQUEST####
                try:
                    html_data = session.post(INTERNAL_QUERY_URL, payload).text
                except:
                    print('Request on first page ({})'.format(BASE_URL + REQUEST))
                    print("Problem of first session.")
                    print('FIELDS:', payload)
                    print('Request on second page ({} with fields {})'.format(INTERNAL_QUERY_URL, payload))
                else:
                    # Create file in html format with this html page request:
                    with open(outhtml, "w", encoding='UTF-8') as ddl:
                        ddl.write(html_data)
                    ddl.close()
                    # Reading this html page:
                    soup = BeautifulSoup(open(outhtml), "html.parser")
                    url = ''
                    for d1 in soup.find_all(href=re.compile("data/download/")):
                        url = 'https://david.ncifcrf.gov/' + str(d1.get("href"))
                        ####TEST URL####
                    try:
                        assert url != ''
                    except:
                        #print ("Not result possible for " + name_PWRN + " , URL is empty because
                        #  David don't find result.")
                        listPWRN.append(name_PWRN)
                    else:
                        result = True
                            ####TEST REQUEST####
                        try:
                            html_data2 = session.post(url).text
                        except:
                            print ("Problem with this session.")
                        else:
                            # Create csv file with the downloaded data from this page:
                            with open(output, "w", encoding='UTF-8') as ddl:
                                ddl.write(html_data2)
                            ddl.close()

        return result, listPWRN

    # Create folders:
    if os.path.exists(folder + "/htmlfile") is False:
        os.makedirs(folder + "/htmlfile")
    if os.path.exists(folder + "/list_David") is False:
        os.makedirs(folder + "/list_David")
    filename_list = []

    # Request:
    REQUEST = 'type=' + annot_type + '&ids=' + listid + '&tool=term2term&annot=' + listannot
    result_request = [False, list_pwrn_david]   # TODO utilite de cette ligne ?

    # Check the length of the request:
    assert len(REQUEST) <= 2048, "Request for David API is superior to 2048 characters"
    html_name = folder + "/htmlfile/htmlDAVID_" + pwrn_name + ".html"
    filename_list.append([folder + "/list_David/List_David_" + pwrn_name + ".csv"])
    result_request = request_david(REQUEST, html_name, filename_list[0][0], pwrn_name,
                                   list_pwrn_david)
    return filename_list, result_request[0], result_request[1]
