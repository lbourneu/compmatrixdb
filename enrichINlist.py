#!/usr/bin/python
#-*- coding: utf-8 -*-
import csv
import os


def open_data(name):
    with open(name, "r") as read_data:
        reader = csv.reader(read_data, delimiter="\t")
        return list(reader)


def enriched(inname, name_pwrn, annot, pv):
    """
    :param inname: input file name
    :param name_pwrn: powernode name
    :param annot: annotation (enrichment score)
    :param pv: p-value
    :return: data_file length, data_file

    Processing of the data file obtained from DAVID.
    Retrieve a list of genes or proteins with a minimum enrichment score and
    a certain p-value.
    A csv file containing the GO term list is created
    """
    # Checking input arguments
    assert type(inname) == str, "The input file is not a string."
    assert inname[-4:] == '.csv', "The input file extension is not '.csv'."
    assert os.path.isfile(inname), "enrichINlist: Input file does not exist!"
    assert type(name_pwrn) == str, "Powernode name is not a string."
    assert type(annot) == float, "Annotation file name is not a float."
    assert annot >= 0.0, "Enrichment score is not a positive number."
    assert type(pv) == float, "p-value is not a float."
    assert pv >= 0.0 and pv <= 1.0, "p-value is not a decimal positive number."
    
    # Retrieve the folder name:
    i = len(inname) - 1
    while inname[i] != '/' and i >= 0:
        i -= 1
    folder = inname[:i]
    
    # Retrieve the data:
    data = open_data(inname)    
    
    # Choose the clusters with better enrichment score than the minimum score:
    cluster = []
    for i in range(len(data)):
        if len(data[i]) > 0:
            donnee = []
            if data[i][0][0:18] == "Annotation Cluster" and float(data[i][1][18:]) >= annot:
                j = i
                while len(data[j]) > 0:
                    donnee.append(data[j])
                    j += 1
                cluster.append(donnee)
    
    # Choose p-value lower than minimum value:
    data_file = []
    for i in range(len(cluster)):
        if cluster[i][1][4] == "PValue":
            for k in range(2, len(cluster[i])):
                if float(cluster[i][k][4]) < pv and not(cluster[i][k][1] in data_file):
                    data_file.append(cluster[i][k][1])
    
    # Writing in new txt file:
    file_name = folder + "/LIST_GOTERM_" + name_pwrn.replace('"', '').replace(' ', '_') + ".csv"
    with open(file_name, "w", encoding='UTF-8') as f:
        for i in range(len(data_file)):
            f.write(str(data_file[i]) + "\n")
    f.close()
    return len(data_file), data_file
